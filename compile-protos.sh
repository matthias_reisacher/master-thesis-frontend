#!/usr/bin/env bash
set -o errexit
set -o nounset
#set -o xtrace

proc_protos() {
    protoc -I=${PROTO_REPO} ${PROTO_REPO}/${1}/*.proto \
    --js_out=import_style=commonjs:${SCR_DIR} \
    --grpc-web_out=import_style=commonjs,mode=grpcwebtext:${SCR_DIR}
}

# TODO: Replace with env-var of Dockerfile
PROTO_REPO=${REPO_PROTO}

if [[ ! -d ${PROTO_REPO} ]]; then
    echo 'Error: Proto repository not found!'
    exit 1
fi

cd $(dirname "$0")
SCR_DIR='./grpc'

[[ -d ${SCR_DIR} ]] && rm -r ${SCR_DIR}
mkdir -p ${SCR_DIR}

proc_protos "utils"
proc_protos "dago-backend"
