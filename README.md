# Setup

Build the example locally with:
```
$ npm run build
$ npm run serve
```
and then visiting http://localhost:8080 in a browser.

The individual steps of WASM build:
```
$ cargo +nightly build --target wasm32-unknown-unknown
$ wasm-bindgen target/wasm32-unknown-unknown/debug/dago.wasm --out-dir ./pkg
```