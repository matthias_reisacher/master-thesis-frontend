require('./index.scss');

(async function ($, window, document) {

    // <editor-fold desc="IMPORTS">
    const dago = await import("../pkg/dago");

    const GraphProtos = await import("../grpc/dago-backend/graph_pb.js");
    const GraphService = await import("../grpc/dago-backend/graph_grpc_web_pb.js");

    const GraphPageProtos = await import("../grpc/dago-backend/graphpage_pb.js");
    const GraphPageService = await import("../grpc/dago-backend/graphpage_grpc_web_pb.js");
    // </editor-fold>

    // <editor-folder desc="CONSTANTS">
    // TODO replace with environment variable and link with docker compose file
    /**
     * The URL of the DAGO backend.
     * @type {string}
     * @const
     */
    const DAGO_BACKEND = "http://localhost:9080";

    /**
     * The size of a single file chunk during uploading in bytes.
     * @type {number}
     * @const
     */
    const CHUNK_SIZE = 512 * 1024;
    // </editor-folder>

    // <editor-fold desc="MAIN FUNCTION">
    /** The main function. */
    $(async () => {
        /** The view controllers **/
        const controlPage = new ControlPage();
        const mainPage = new MainPage();
        const fileUploadPage = new FileUploadPage();
        const addGraphPage = new AddGraphPage();

        /** Initialize page views **/
        controlPage.init();
        mainPage.init();
        mainPage.showRenderControls();

        /** The frontend service. */
        const client = new WebClient();

        /** The backend service. */
        const backend = new DagoBackend(DAGO_BACKEND);
        await initGraphSelectors();

        /**
         * Holds all available graphs.
         * @type{Graphs}
         */
        let graphs;

        /**
         * Holds the currently visualized graph.
         * @type{Graphs.Graph}
         */
        let currentGraph;

        /**
         * Holds the currently visualized graph's page.
         * @type{GraphPage}
         */
        let currentGraphPage;

        /**
         * Holds the number of available pages for the current graph.
         * @type{number}
         */
        let currentGraphNAvailablePages;

        /**
         * The "right" requestAnimationFrame function of the used browser.
         * @type {function}
         */
        const requestAnimationFrame =
            window.requestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.msRequestAnimationFrame;

        /**
         * Stores the request graphId of the animated rendering request.
         * @type {number}
         */
        let animationFrameId = -1;

        /** Requests browser to render the canvas on every repaint. */
        function startAnimatedRendering() {
            client.render();
            animationFrameId = requestAnimationFrame(startAnimatedRendering);
        }

        /** Stops the browser from rendering the canvas on every repaint. */
        function stopAnimatedRendering() {
            cancelAnimationFrame(animationFrameId);
        }

        /**
         * Calculates the available screen space for the canvas.
         * Returns an array with 2 values: [width, height].
         * @return {number[]}
         */
        function calcCanvasViewport() {
            const rendererMarginHorizontal = 15;

            const depthSelectorColumnWidth = $('#renderer-page-depth-selector').outerWidth(true);
            const mainHeaderHeight = $('#main-header').outerHeight(true);
            const rendererHeaderHeight = $('#renderer-header-div').outerHeight(true);
            const rendererPageSelectorHeight = $('#renderer-page-selector').outerHeight(true);

            const renderWidth = window.innerWidth - depthSelectorColumnWidth - 2 * rendererMarginHorizontal;
            const renderHeight = window.innerHeight - mainHeaderHeight - rendererHeaderHeight - rendererPageSelectorHeight;

            return [Math.floor(renderWidth), Math.floor(renderHeight)];
        }

        /**
         * Fetches the number of graph pages for the given graph and sets the settings for the slider element accordingly.
         * @param graphId
         * @return {Promise<void>}
         */
        function setNumberOfGraphPages(graphId) {
            return new Promise((resolve, reject) => {
                getPageCountOfGraph(graphId)
                    .then(counter => {
                        currentGraphNAvailablePages = counter;
                        mainPage.setPageSliderLimits(currentGraphNAvailablePages);

                        resolve();
                    })
                    .catch(err => {
                        console.error("Error on setting current graph page");
                        reject();
                    })
            });
        }

        /**
         * Fetches and sets the current graph page.
         * @param graphId {number} - The ID of the graph
         * @param number {number} - The number of the graph's page
         * @param depth {number} - The depth of the page
         * @return {Promise<void>}
         */
        async function setGraphPageForRendering(graphId, number, depth) {
            validatePageDepthOfNextPage(graphId, number, depth)
                .then(([validatedDepth, maxDepth]) => {
                    mainPage.setDepthSliderLimits(maxDepth);

                    backend.fetchGraphPage(graphId, number, validatedDepth)
                        .then(jsonData => {
                            // Update current graph page
                            currentGraphPage = new GraphPage(jsonData);

                            // Show rendering view
                            mainPage.showRenderer(currentGraph.name, currentGraph.description, number, validatedDepth);
                            const canvasViewport = calcCanvasViewport();

                            // Set canvas size
                            mainPage.setCanvasSize(canvasViewport[0], canvasViewport[1]);

                            // Set data and start rendering
                            client.setDimensions(canvasViewport[0], canvasViewport[1]);
                            client.setData(currentGraphPage.vertices, currentGraphPage.edges);
                            client.render();
                        })
                        .catch(err => {
                            currentGraphPage = null;
                            console.error("Error on setting current graph page", err);
                        });
                });
        }

        /**
         * Validates the given depth. If depth is larger than the maximum depth of the specified page,
         * it is set to the largest possible depth value.
         * Returns an array containing the validated depth value and the fetched max depth.
         * @param graphId {number} - The ID of the graph
         * @param number {number} - The number of the graph's page
         * @param depth {number} - The depth of the page
         * @return {Promise<[number]>}
         */
        function validatePageDepthOfNextPage(graphId, number, depth) {
            return new Promise((resolve, reject) => {
                getDepthOfGraphPage(graphId, number)
                    .then(maxDepth => {
                        let validatedDepth = (depth > maxDepth) ? maxDepth : depth;
                        resolve([validatedDepth, maxDepth]);
                    })
                    .catch(() => {
                        console.warn("Could not calculate page depth of next page");
                        reject();
                    });
            });
        }

        /**
         * Fetches the page counter of the specified graph.
         * @param graphId
         * @return {Promise<number>}
         */
        function getPageCountOfGraph(graphId) {
            // Since the FileReader API does not support async/await, a Promise wrapper must be used.
            return new Promise(resolve => {
                backend.getPageCount(graphId)
                    .then(counter => {
                        resolve(counter);
                    })
                    .catch(_ => {
                        resolve(0);
                    });
            });
        }

        /**
         * Fetches the maximum depth of the specified graph page.
         * @param graphId {number}
         * @param pageNumber {number}
         * @return {Promise<number>}
         */
        function getDepthOfGraphPage(graphId, pageNumber) {
            return new Promise((resolve, reject) => {
                backend.getPageDepth(graphId, pageNumber)
                    .then(depth => {
                        if (depth > 0) {
                            resolve(depth);
                        } else {
                            reject();
                        }
                    })
                    .catch(() => {
                        reject();
                    });
            });
        }

        /**
         * Sends a request to create the new graph.
         * Navigates on success to the page upload view.
         * If one of the parameters is undefined, null or an empty string, nothing is done.
         * @param name {string}
         * @param description {string}
         */
        function createNewGraph(name, description) {
            if (name !== undefined && name != null && name.length > 0 &&
                description !== undefined && description != null && description.length > 0) {

                backend.createGraph(name, description)
                    .then(async () => {
                        await initGraphSelectors();

                        controlPage.showUploadButton();
                        addGraphPage.hide();
                        fileUploadPage.show();

                        $('#add-graph-name').val('');
                        $('#add-graph-description').val('');
                    });
            } else {
                console.warn("Invalid graph name and/or description - cannot create graph.");
            }
        }

        /**
         * The file upload function triggered by a HTML input element.
         * @return {Promise<void>}
         */
        async function uploadFiles() {
            const files = this.files;

            const graphId = Number($("#graph-selector-upload").val());
            if (isNaN(graphId)) {
                console.error("Invalid graph graphId '" + graphId + "': Canceling file upload.");
                return;
            }

            fileUploadPage.showUploadingSign();
            console.info("Starting file upload for graph '" + graphId + "' ...");
            console.info("Uploading " + files.length + " files ...");
            for (let i = 0; i < files.length; i++) {
                await uploadFile(graphId, files[i]);
                console.info("... uploaded " + (i + 1) + " of " + files.length + " files.")
            }
            fileUploadPage.showUploadInput();
        }

        /**
         * Processes the upload of a single file to the backend for a specific graph.
         * @param graphId {number} - The ID of the graph.
         * @param file {file} - The file to process
         * @return {Promise<void>}
         */
        async function uploadFile(graphId, file) {
            // Convert file into binary string
            const binaryString = await convertFileIntoBinaryString(file);

            // Initialize file upload at server side
            const jsonData = await backend.initFileUpload(graphId, file.name, file.type, binaryString.length);
            const fileUpload = new InitFileUpload(jsonData);

            // Upload binary string
            await uploadBinaryString(fileUpload.id, btoa(binaryString), 0);
        }

        /**
         * Converts the given file into a binary string.
         * @param file {file} - The file to convert
         * @return {Promise<String>}
         */
        function convertFileIntoBinaryString(file) {
            // Since the FileReader API does not support async/await, a Promise wrapper must be used.
            return new Promise((resolve, reject) => {
                let reader = new FileReader();

                reader.onerror = reject;
                reader.onload = function () {
                    const binaryString = btoa(new Uint8Array(this.result).reduce((data, byte) => {
                        return data + String.fromCharCode(byte);
                    }, ''));

                    resolve(binaryString);
                };

                reader.readAsArrayBuffer(file);
            })
        }

        /**
         * Splits the binary string into a sequence of chunks with the size {@link CHUNK_SIZE}
         * and uploads each individual chunk to the backend.
         * @param id {string} - The graphId of the file upload
         * @param binaryString {string} - The binary string to upload
         * @param start {number} - The starting position of the sequence (included)
         * @param number {number} - The number of the chunk
         * @return {Promise<void>}
         */
        async function uploadBinaryString(id, binaryString, start, number = 0) {
            if (start < binaryString.length) {
                let chunk;
                let end = start + CHUNK_SIZE;

                if (end < binaryString.length) {
                    chunk = binaryString.slice(start, end);
                } else {
                    chunk = binaryString.slice(start);
                    end = binaryString.length;
                }

                await backend.chunkFileUpload(id, number, chunk)
                    .then(async () => {
                        await uploadBinaryString(id, binaryString, end, ++number);
                    });
            } else {
                const jsonData = await backend.finalizeFileUpload(id);
                const result = new FinalizeFileUpload(jsonData);

                if (!result.success) {
                    console.error("An error occurred during the file upload.")
                }
            }
        }

        /**
         * Loads all graphs and fills the selector input.
         * @return {Promise<void>}
         */
        function initGraphSelectors() {
            backend.fetchGraphs()
                .then(fetchedGraphs => {
                    graphs = new Graphs(fetchedGraphs);

                    // Clear selectors
                    $('#graph-selector-render')
                        .find('option')
                        .remove();
                    $('#graph-selector-upload')
                        .find('option')
                        .remove();

                    // Add option for each graph
                    graphs.graphs.forEach((graph) => {
                        $('#graph-selector-render').append('<option value="' + graph.id + '">' + graph.name + '</option>');
                        $('#graph-selector-upload').append('<option value="' + graph.id + '">' + graph.name + '</option>');
                    });
                });
        }

        /**
         * Searches the fetched graphs for a graph with the given graphId and returns it.
         * @param graphId {number|string}
         * @return {Graphs.Graph|null}
         */
        function getGraphById(graphId) {
            for (let i = 0; i < graphs.graphs.length; i++) {
                if (graphs.graphs[i].id == graphId) {
                    return graphs.graphs[i];
                }
            }

            return null;
        }

        $("#control-add").click(() => {
            controlPage.showBackButton();
            mainPage.hide();
            addGraphPage.show();
        });

        $("#control-upload").click(() => {
            controlPage.showBackButton();
            mainPage.hide();
            fileUploadPage.show();
        });

        $("#control-back").click(() => {
            controlPage.init();
            mainPage.showRenderControls();
            addGraphPage.hide();
            fileUploadPage.hide();
        });

        // <editor-fold desc="HTML Action Bindings">
        $("#renderer-start").click(() => {
            controlPage.showStopButton();

            const graphId = $('#graph-selector-render').val();
            currentGraph = getGraphById(graphId);
            console.log("Selected graph for rendering", currentGraph);

            setNumberOfGraphPages(graphId).then(async () => {
                await setGraphPageForRendering(graphId, 1, 1);
            });
        });

        $("#renderer-stop").click(() => {
            controlPage.init();
            mainPage.showRenderControls();
        });

        $("#dago")
            .mousedown(startAnimatedRendering)
            .mouseup(stopAnimatedRendering).bind('wheel', client.render);

        $("#file-input").on('change', uploadFiles);

        $('#renderer-graph-page-input')
            .on('input', function (e) {
                $('#render-graph-page-span').html(e.target.value);
            })
            .on('change', async function (e) {
                await setGraphPageForRendering(currentGraphPage.graphId, e.target.value, currentGraphPage.depth);
            });

        $('#renderer-graph-depth-input')
            .on('input', function (e) {
                $('#render-graph-depth-span').html(e.target.value);
            })
            .on('change', async function (e) {
                await setGraphPageForRendering(currentGraphPage.graphId, currentGraphPage.number, e.target.value);
            });

        $('#add-new-graph').click(function () {
            const name = $('#add-graph-name').val();
            const description = $('#add-graph-description').val();

            createNewGraph(name, description);
        });
        // </editor-fold>
    });

    // </editor-fold>

    // <editor-fold desc="PAGING CONTROLLER">
    /**
     * Page flow controller of the control view.
     * @constructor
     */
    function ControlPage() {
        function hideAll() {
            $('#control-add').addClass("hidden");
            $('#control-upload').addClass("hidden");
            $('#control-back').addClass("hidden");
            $('#renderer-start').addClass("hidden");
            $('#renderer-stop').addClass("hidden");
        }

        /** Shows the edit and play buttons */
        this.init = function () {
            hideAll();
            $('#control-buttons').removeClass("hidden");
            $('#control-add').removeClass("hidden");
            $('#control-upload').removeClass("hidden");
            $('#renderer-start').removeClass("hidden");
        };

        /** Shows only the stop button */
        this.showStopButton = function () {
            hideAll();
            $('#renderer-stop').removeClass("hidden");
        };

        /** Shows only the back button */
        this.showBackButton = function () {
            hideAll();
            $('#control-back').removeClass("hidden");
        };

        /** Shows only the upload button */
        this.showUploadButton = function () {
            hideAll();
            $('#control-upload').removeClass("hidden");
        };
    }

    /**
     * Page flow controller of the main view.
     * @constructor
     */
    function MainPage() {
        /** Helper function to hides all elements */
        function hideAll() {
            $('#render-settings').addClass("hidden");
            $('#renderer').addClass("hidden");
        }

        /** Shows the main page */
        this.init = function () {
            $('#main-page').removeClass("hidden");
            hideAll();
        };

        /** Hides the main page */
        this.hide = function () {
            hideAll();
        };

        /** Shows only the render controls */
        this.showRenderControls = function () {
            hideAll();
            $('#render-settings').removeClass("hidden");
        };

        /**
         * Shows only the render controls
         * @param name {string} - The name of the rendered graph.
         * @param description {string} - The description of the rendered graph.
         * @param number {number} - The number of the rendered graph page.
         * @param currentDepth {number} - The depth of the currently shown page.
         */
        this.showRenderer = function (name, description, number, currentDepth) {
            // Write selected graph name into renderer header
            $('#renderer-header').html(name);
            $('#renderer-graph-description').html(description);

            // Update depth slider
            $('#render-graph-depth-span').html(currentDepth);
            $('#renderer-graph-depth-input').val(currentDepth);

            // Update page slider
            $('#render-graph-page-span').html(number);
            $('#renderer-graph-page-input').val(number);

            hideAll();
            $('#renderer').removeClass("hidden");
        };

        /**
         * Sets the max value of the page slider.
         * If the max value is equal to one, the page slider is hidden.
         * @param max {number|string}
         */
        this.setPageSliderLimits = function (max) {
            const input = $('#renderer-graph-page-input');
            input.attr("max", max);

            // Hide slider if only a single page is available
            if (max == 1) {
                input.addClass("hidden");
            } else {
                input.removeClass("hidden");
            }
        };

        /**
         * Sets the max value of the depth slider.
         * If the max value is equal to one, the depth slider is hidden.
         * @param max {number|string}
         */
        this.setDepthSliderLimits = function (max) {
            const input = $('#renderer-graph-depth-input');
            input.attr("max", max);

            // Hide slider if only a single page is available
            if (max == 1) {
                input.addClass("hidden");
            } else {
                input.removeClass("hidden");
            }
        };

        this.setCanvasSize = function (width, height) {
            const canvas = $('#dago-canvas');
            canvas.attr("width", width);
            canvas.attr("height", height);
        };
    }

    /**
     * Page flow controller of the add graph view.
     * @constructor
     */
    function AddGraphPage() {
        /** Shows the add graph view */
        this.show = function () {
            $('#add-graph-page').removeClass("hidden");
        };

        /** Hides the add graph view */
        this.hide = function () {
            $('#add-graph-page').addClass("hidden");
        };
    }

    /**
     * Page flow controller of the file upload view.
     * @constructor
     */
    function FileUploadPage() {
        /** Shows the file upload view */
        this.show = function () {
            $('#file-upload-page').removeClass("hidden");
        };

        /** Hides the file upload view */
        this.hide = function () {
            $('#file-upload-page').addClass("hidden");
        };

        /** Shows the upload input field and hides the uploading spinner */
        this.showUploadInput = function () {
            $('#file-upload-input').removeClass("hidden");
            $('#file-upload-loading').addClass("hidden");
        };

        /** Shows the uploading spinner and hides the upload input field */
        this.showUploadingSign = function () {
            $('#file-upload-input').addClass("hidden");
            $('#file-upload-loading').removeClass("hidden");
        };
    }

    // </editor-fold>


    // <editor-fold desc="MODEL HOLDING GRAPH-PAGE DATA">
    /**
     * Holding the DAGO Graph data.
     * @param response {GraphProtos.GraphResponse}
     * @constructor
     */
    function Graphs(response) {
        /**
         * Holding the data of a single graph.
         * @param graph {GraphProtos.GraphResponse.Graph}
         * @constructor
         */
        function Graph(graph) {
            /**
             * The ID of the graph.
             * @type{number}
             */
            this.id = graph.getId();

            /**
             * The name of the graph.
             * @type{string}
             */
            this.name = graph.getName();

            /**
             * The description of the graph.
             * @type{string}
             */
            this.description = graph.getDescription();
        }

        /**
         * All available graphs.
         * @type{Array<Graph>}
         */
        this.graphs = [];

        /**
         * Creates Graphs from an Array.
         * @param graphs {Array}
         */
        this.initGraphs = function (graphs) {
            graphs.forEach((graph) => {
                this.graphs.push(new Graph(graph));
            });
        };

        this.initGraphs(response.getGraphList());
    }

    /**
     * Holding the DAGO GraphPage data.
     * @param response {GraphPageProtos.GraphPageResponse}
     * @constructor
     */
    function GraphPage(response) {
        /**
         * The ID of the graph.
         * @type{number}
         */
        this.graphId = response.getGraphPage().getGraphId();
        /**
         * The number of the page.
         * @type{number}
         */
        this.number = response.getGraphPage().getPageNumber();
        /**
         * The depth of the page.
         * @type{number}
         */
        this.depth = response.getGraphPage().getPageDepth();
        /**
         * The list of vertices. Two successive values for x and y define a single vertex.
         * @type{Float32Array}
         */
        this.vertices = new Float32Array(response.getGraphPage().getVertexList());
        /**
         * The list of vertices. Four successive values for start-x/y and end-x/y define a single edge.
         * @type{Float32Array}
         */
        this.edges = new Float32Array(response.getGraphPage().getEdgeList());
    }

    /**
     * Holding the DAGO FileUpload initialization data.
     * @param response {GraphPageProtos.InitUploadResponse}
     * @constructor
     */
    function InitFileUpload(response) {
        /**
         * The ID of the file upload.
         * @type {string}
         */
        this.id = response.getId();
    }

    /**
     * Holding the DAGO FileUpload result data.
     * @param response {GraphPageProtos.FinalizeUploadResponse}
     * @constructor
     */
    function FinalizeFileUpload(response) {
        /**
         * The result of the file upload.
         * @type {boolean}
         */
        this.success = response.getResult();
    }

    // </editor-fold>

    // <editor-fold desc="SERVICE CLASS FOR WEB CLIENT">
    /**
     * A wrapper for the WASM-library.
     * @constructor
     */
    function WebClient() {
        const client = new dago.WebClient();
        client.start();

        let viewportWidth = null;
        let viewportHeight = null;

        /** Renders the scene. */
        this.render = () => {
            try {
                client.render(viewportWidth, viewportHeight);
            } catch (e) {
                console.error(e);
            }
        };

        /**
         * Passes the node data to WASM.
         * @param {Float32Array} nodes - Node data
         * @param {Float32Array} edges - Edge data
         */
        this.setData = (nodes, edges) => {
            try {
                client.set_data(nodes, edges);
            } catch (e) {
                console.error(e);
            }
        };

        /**
         * Renders the scene for the given viewport size.
         * If no viewport size is specified, the default value is used during rendering.
         * @param width {number|null} - Positive, whole number
         * @param height {number|null} - Positive, whole number
         */
        this.setDimensions = (width, height) => {
            viewportWidth = width;
            viewportHeight = height;

            client.update_canvas_size(width, height);
        };
    }

    //</editor-fold>

    // <editor-fold desc="SERVICE CLASS FOR AJAX CALLS TO DAGO BACKEND">
    /**
     * A wrapper for gRPC calls to the DAGO-backend.
     * @param backendUrl {string} - URL of DAGO-backend
     * @constructor
     */
    function DagoBackend(backendUrl) {
        const graphService = new GraphService.GraphServiceClient(backendUrl);
        const graphPageService = new GraphPageService.GraphPageServiceClient(backendUrl);

        /**
         * Fetches all graphs.
         * @return {Promise<GraphResponse>}
         */
        this.fetchGraphs = () => {
            return new Promise((resolve, reject) => {
                graphService.fetchAll(new GraphProtos.Empty(), {}, (err, response) => {
                    if (err) {
                        console.error("FetchGraphs - Error", err.code, err.message);
                        reject();
                    } else {
                        console.debug("FetchGraphs - Response", response);
                        resolve(response);
                    }
                });
            });
        };

        /**
         * Fetches the page of a given depth from the server.
         * @param graphId {number} - The ID of the graph
         * @param number {number} - The number of the graph's page
         * @param depth {number} - The depth of the page
         * @return {Promise<GraphPageProtos.GraphPageResponse>} - The fetched graph's page when resolved
         */
        this.fetchGraphPage = (graphId, number, depth) => {
            const request = new GraphPageProtos.GraphPageRequest();
            request.setGraphId(graphId);
            request.setPageNumber(number);
            request.setPageDepth(depth);

            return new Promise((resolve, reject) => {
                console.debug("FetchGraphPage - Request", request);
                graphPageService.fetch(request, {}, (err, response) => {
                    if (err) {
                        console.error("FetchGraphPage - Error", err.code, err.message);
                        reject();
                    } else {
                        console.debug("FetchGraphPage - Response", response);
                        resolve(response);
                    }
                });
            });
        };

        /**
         * Fetches the page count of the given graph.
         * @param graphId {number}
         * @return {Promise<number>}
         */
        this.getPageCount = (graphId) => {
            const request = new GraphPageProtos.PageCountRequest();
            request.setGraphId(graphId);

            return new Promise((resolve, reject) => {
                console.debug("GetPageCount - Request", request);
                graphPageService.getPageCount(request, {}, (err, response) => {
                    if (err) {
                        console.error("GetPageCount - Error", err.code, err.message);
                        reject();
                    } else {
                        console.debug("GetPageCount - Response", response);
                        resolve(response.getCounter());
                    }
                });
            });
        };

        /**
         * Fetches the maximal depth of the respective graph page.
         * @param graphId {number}
         * @param pageNumber {number}
         * @return {Promise<number>}
         */
        this.getPageDepth = (graphId, pageNumber) => {
            const request = new GraphPageProtos.PageDepthRequest();
            request.setGraphId(graphId);
            request.setPageNumber(pageNumber);

            return new Promise((resolve, reject) => {
                console.debug("GetPageDepth - Request", request);
                graphPageService.getPageDepth(request, {}, (err, response) => {
                    if (err) {
                        console.error("GetPageDepth - Error", err.code, err.message);
                        reject();
                    } else {
                        console.debug("GetPageDepth - Response", response);
                        resolve(response.getDepth());
                    }
                });
            });
        };

        /**
         * Creates a new graph.
         * @param name {string} - The name of the graph.
         * @param description {string} - The description of the graph.
         * @return {Promise<GraphProtos.CreateGraphResponse>}
         */
        this.createGraph = function (name, description) {
            const request = new GraphProtos.CreateGraphRequest();
            request.setName(name);
            request.setDescription(description);

            return new Promise((resolve, reject) => {
                console.debug("CreateGraph - Request", request);
                graphService.createGraph(request, {}, (err, response) => {
                    if (err) {
                        console.error("CreateGraph - Error", err);
                        reject();
                    } else {
                        console.debug("CreateGraph - Response", response);
                        resolve(response);
                    }
                });
            });
        };

        /**
         * Initializes a new file upload.
         * @param graphId {number} - The ID of the graph.
         * @param size {number} - The file size
         * @param type {string} - The file type
         * @param name {string} - The file name
         * @return {Promise<GraphPageProtos.InitUploadResponse>} - The received upload response when resolved
         */
        this.initFileUpload = (graphId, name, type, size) => {
            const request = new GraphPageProtos.InitUploadRequest();
            request.setGraphId(graphId);
            request.setName(name);
            request.setType(type);
            request.setSize(size);

            return new Promise((resolve, reject) => {
                console.debug("InitFileUpload - Request", request);
                graphPageService.initUpload(request, {}, (err, response) => {
                    if (err) {
                        console.error("InitFileUpload - Error", err.code, err.message);
                        reject();
                    } else {
                        console.debug("InitFileUpload - Response", response);
                        resolve(response);
                    }
                });
            });
        };

        /**
         * Uploads a file chunk as part of a file upload.
         * @param id {string} - The ID of the file upload
         * @param number {number} - The number of the chunk
         * @param chunk {string} - A part of the file represented by a byte-string
         * @return {Promise<GraphPageProtos.Empty>}
         */
        this.chunkFileUpload = (id, number, chunk) => {
            const request = new GraphPageProtos.ChunkUploadRequest();
            request.setId(id);
            request.setNumber(number);
            request.setChunk(chunk);

            return new Promise((resolve, reject) => {
                console.debug("ChunkFileUpload - Request", request);
                graphPageService.chunkUpload(request, {}, (err, response) => {
                    if (err) {
                        console.error("ChunkFileUpload - Error", err.code, err.message);
                        reject();
                    } else {
                        console.debug("ChunkFileUpload - Response", response);
                        resolve();
                    }
                });
            });
        };

        /**
         * Finalizes the file upload.
         * @param id {string} - The ID of the file upload
         * @return {Promise<GraphPageProtos.FinalizeUploadResponse>}
         */
        this.finalizeFileUpload = (id) => {
            const request = new GraphPageProtos.FinalizeUploadRequest();
            request.setId(id);

            return new Promise((resolve, reject) => {
                console.debug("FinalizeFileUpload - Request", request);
                graphPageService.finalizeUpload(request, {}, (err, response) => {
                    if (err) {
                        console.error("FinalizeFileUpload - Error", err.code, err.message);
                        reject();
                    } else {
                        console.debug("FinalizeFileUpload - Response", response);
                        resolve(response);
                    }
                });
            });
        };
    }

    // </editor-fold>
})(jQuery, window, document);