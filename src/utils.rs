use std::panic;

/// Sets the panic hook to the web browser javascript console.
pub fn set_panic_hook() {
    panic::set_hook(Box::new(console_error_panic_hook::hook));
}

pub mod js {
    use wasm_bindgen::prelude::*;

    #[wasm_bindgen]
    extern "C" {
        #[wasm_bindgen(js_name = alert)]
        fn js_alert(s: &str);

        #[wasm_bindgen(js_namespace = console, js_name = debug)]
        fn js_debug(s: &str);

        #[wasm_bindgen(js_namespace = console, js_name = log)]
        fn js_log(s: &str);

        #[wasm_bindgen(js_namespace = console, js_name = warn)]
        fn js_warn(s: &str);

        #[wasm_bindgen(js_namespace = console, js_name = error)]
        fn js_error(s: &str);
    }

    /// Logs the message to the web browser javascript console.
    /// Usage:
    /// ```
    /// alert(&format!("Hello, {}!", name));
    /// ```
    #[allow(dead_code)]
    #[allow(unused_unsafe)]
    pub fn alert(message: &str) {
        unsafe {
            js_alert(message);
        }
    }

    #[allow(dead_code)]
    #[allow(unused_unsafe)]
    pub fn debug(message: &str) {
        unsafe {
            js_debug(message);
        }
    }

    #[allow(dead_code)]
    #[allow(unused_unsafe)]
    pub fn log(message: &str) {
        unsafe {
            js_log(message);
        }
    }

    #[allow(dead_code)]
    #[allow(unused_unsafe)]
    pub fn warn(message: &str) {
        unsafe {
            js_warn(message);
        }
    }

    #[allow(dead_code)]
    #[allow(unused_unsafe)]
    pub fn error(message: &str) {
        unsafe {
            js_error(message);
        }
    }
}