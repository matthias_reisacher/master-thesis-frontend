use std::f32::consts::PI;

use nalgebra::{Isometry3, Perspective3, Point3, Vector3};

static CAMERA_DIST_START: f32 = -5.;
static CAMERA_DIST_MIN: f32 = -0.1;
static CAMERA_DIST_MAX: f32 = -30.;

static MOVEMENT_FACTOR: f32 = 0.1;
static ZOOM_FACTOR: f32 = 0.6;

pub struct Camera {
    position: Point3<f32>,
    projection: Perspective3<f32>,
}

impl Camera {
    /// Creates a new Camera with the given aspect ratio
    pub fn new(aspect: f32) -> Camera {
        let fov = PI / 3.;
        let z_near = 1.;
        let z_far = 50.;

        Camera {
            position: Point3::new(0., 0., CAMERA_DIST_START),
            projection: Perspective3::new(aspect, fov, z_near, z_far),
        }
    }

    /// Calculates and returns the View Matrix
    pub fn view(&self) -> Isometry3<f32> {
        let eye = &self.position;
        let target = Point3::new(eye.x, eye.y, 0.);
        Isometry3::look_at_rh(&eye, &target, &Vector3::y())
    }

    /// Returns the Projection Matrix
    pub fn projection(&self) -> &Perspective3<f32> {
        &self.projection
    }

    /// Horizontal movement for user input
    pub fn move_left_right(
        &mut self,
        delta: f32,
    ) {
        self.position.x -= delta * MOVEMENT_FACTOR;
    }

    /// Vertical movement for user input
    pub fn move_up_down(
        &mut self,
        delta: f32,
    ) {
        self.position.y += delta * MOVEMENT_FACTOR;
    }

    /// Zoom movement for user input
    pub fn zoom(
        &mut self,
        zoom: f32,
    ) {
        self.position.z += zoom * ZOOM_FACTOR;

        if self.position.z < CAMERA_DIST_MAX {
            self.position.z = CAMERA_DIST_MAX;
        } else if self.position.z > CAMERA_DIST_MIN {
            self.position.z = CAMERA_DIST_MIN;
        }
    }
}