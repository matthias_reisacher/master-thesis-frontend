use crate::canvas::{CANVAS_HEIGHT, CANVAS_WIDTH};

use self::camera::Camera;
use self::mouse::Mouse;

mod camera;
mod mouse;

pub struct State {
    camera: Camera,
    mouse: Mouse
}

impl State {
    /// Creates a new State
    pub fn new() -> State {
        let aspect = CANVAS_WIDTH as f32 / CANVAS_HEIGHT as f32;

        State {
            camera: Camera::new(aspect),
            mouse: Mouse::default(),
        }
    }

    /// Creates a new camera with the given aspect ratio
    pub fn new_camera(
        &mut self,
        aspect: f32
    ) {
        self.camera = Camera::new(aspect)
    }

    /// Returns an immutable borrowed reference
    pub fn camera(&self) -> &Camera {
        &self.camera
    }

    /// Messaging system to change the current state
    pub fn msg(&mut self, msg: &Msg) {
        match msg {
            Msg::MouseDown(x, y) => {
                self.mouse.set_pressed(true);
                self.mouse.set_pos(*x, *y);
            }
            Msg::MouseUp => {
                self.mouse.set_pressed(false);
            }
            Msg::MouseMove(x, y) => {
                if !self.mouse.get_pressed() {
                    return;
                }

                let (old_x, old_y) = self.mouse.get_pos();

                let x_delta = old_x as i32 - x;
                let y_delta = y - old_y as i32;

                self.camera.move_left_right(x_delta as f32);
                self.camera.move_up_down(y_delta as f32);

                self.mouse.set_pos(*x, *y);
            }
            Msg::Zoom(zoom) => {
                self.camera.zoom(*zoom);
            }
        }
    }
}

pub enum Msg {
    MouseDown(i32, i32),
    MouseUp,
    MouseMove(i32, i32),
    Zoom(f32),
}