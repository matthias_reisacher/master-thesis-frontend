use crate::app::data::preprocessor::{DrawableEdges, DrawableNodes, process_edges, process_nodes};
use crate::render::render_edges::calc_edge_vertices;
use crate::render::render_nodes::{get_triangle_vertices, TEX_COORDS};

pub mod preprocessor;

pub struct Graph {
    nodes: Nodes,
    edges: Edges,
}

impl Graph {
    /// Creates a new Graph
    pub fn new() -> Graph {
        Graph {
            nodes: Nodes::new(),
            edges: Edges::new(),
        }
    }

    /// Getter for nodes
    pub fn get_nodes(&self) -> &Nodes {
        &self.nodes
    }

    /// Getter for edges
    pub fn get_edges(&self) -> &Edges {
        &self.edges
    }

    /// Sets the data
    pub fn set_data(&mut self, node_data: Vec<f32>, edge_data: Vec<f32>) {
        self.nodes.set_data(node_data);
        self.edges.set_data(edge_data);
    }

    /// Removes all data from the graph
    pub fn clear(&mut self) {
        self.nodes.clear();
        self.edges.clear();
    }
}

/// Convenience structure to encapsulate the node data
pub struct Nodes {
    data: Option<DrawableNodes>
}

impl Nodes {
    /// Creates new Nodes
    fn new() -> Nodes {
        Nodes { data: None }
    }

    /// Preprocesses the given data and stores them afterwards
    fn set_data(&mut self, data: Vec<f32>) {
        let triangle_vertices;
        unsafe {
            triangle_vertices = get_triangle_vertices();
        }
        let drawable = process_nodes(&data, triangle_vertices,
                                     &TEX_COORDS);
        self.data = Some(drawable);
    }

    /// Getter for drawable
    pub fn get_data(&self) -> &Option<DrawableNodes> {
        &self.data
    }

    /// Sets the data vector to `None`
    fn clear(&mut self) {
        if self.data.is_some() {
            self.data = None;
        }
    }
}

/// Convenience structure to encapsulate the edge data
pub struct Edges {
    data: Option<DrawableEdges>
}

impl Edges {
    /// Creates new Nodes
    fn new() -> Edges {
        Edges { data: None }
    }

    /// Preprocesses the given data and stores them afterwards
    fn set_data(&mut self, data: Vec<f32>) {
        let drawable = process_edges(&data, &calc_edge_vertices);
        self.data = Some(drawable);
    }

    /// Getter for drawable
    pub fn get_data(&self) -> &Option<DrawableEdges> {
        &self.data
    }

    /// Sets the data vector to `None`
    fn clear(&mut self) {
        if self.data.is_some() {
            self.data = None;
        }
    }
}