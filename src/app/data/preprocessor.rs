#[derive(Debug)]
pub struct DrawableNodes {
    vertices: Vec<f32>,
    indices: Vec<u16>,
    tex_coords: Vec<f32>,
}

impl DrawableNodes {
    pub fn get_vertices(&self) -> &Vec<f32> {
        &self.vertices
    }

    pub fn get_indices(&self) -> &Vec<u16> {
        &self.indices
    }

    pub fn get_tex_coords(&self) -> &Vec<f32> {
        &self.tex_coords
    }
}

impl Default for DrawableNodes {
    fn default() -> Self {
        DrawableNodes {
            vertices: vec![],
            indices: vec![],
            tex_coords: vec![],
        }
    }
}

#[derive(Debug)]
pub struct DrawableEdges {
    vertices: Vec<f32>,
    indices: Vec<u16>,
}

impl DrawableEdges {
    pub fn get_edges(&self) -> &Vec<f32> {
        &self.vertices
    }

    pub fn get_indices(&self) -> &Vec<u16> {
        &self.indices
    }
}

impl Default for DrawableEdges {
    fn default() -> Self {
        DrawableEdges {
            vertices: vec![],
            indices: vec![],
        }
    }
}

/// Places a triangle at each node position.
/// Returns a DrawableElement containing the resulting positions, indices and texture coordinates.
pub fn process_nodes(
    nodes: &Vec<f32>,
    triangle_dimensions: &[f32],
    triangle_tex_coords: &[f32],
) -> DrawableNodes {
    let mut drawable = create_drawable_nodes(nodes);
    fill_drawable_nodes(&mut drawable, nodes, triangle_dimensions, triangle_tex_coords);
    drawable
}

/// Places two triangles at each edge position.
/// Returns a DrawableElement containing the resulting positions and indices.
pub fn process_edges(
    edges: &Vec<f32>,
    calc_square_dimension: &Fn(f32, f32, f32, f32) -> [f32; 8],
) -> DrawableEdges {
    let mut drawable = create_drawable_edges(edges);
    fill_drawable_edges(&mut drawable, edges, calc_square_dimension);
    drawable
}

/// Creates vectors large enough to store the vertex, index and texture data for the node data set.
///
/// # Returns
/// 1) vertex positions
/// 2) indices
/// 3) texture coordinates
fn create_drawable_nodes(node_data: &Vec<f32>) -> DrawableNodes {
    let n_vertices = node_data.len() * 3;
    let n_indices = f32::ceil(node_data.len() as f32 * 1.5f32) as usize;

    let vertices: Vec<f32> = Vec::with_capacity(n_vertices);
    let indices: Vec<u16> = Vec::with_capacity(n_indices);
    let tex_coords: Vec<f32> = Vec::with_capacity(n_vertices);

    DrawableNodes { vertices, indices, tex_coords }
}

/// Creates vectors large enough to store the vertex and index data for the edge data set.
///
/// # Returns
/// 1) vertex positions
/// 2) indices
fn create_drawable_edges(edge_data: &Vec<f32>) -> DrawableEdges {
    let n_vertices = edge_data.len() * 2;
    let n_indices = f32::ceil(edge_data.len() as f32 * 1.5f32) as usize;

    let vertices: Vec<f32> = Vec::with_capacity(n_vertices);
    let indices: Vec<u16> = Vec::with_capacity(n_indices);

    DrawableEdges { vertices, indices }
}

/// Fills the vectors with the vertices and indices from the nodes data set
fn fill_drawable_nodes(
    drawable: &mut DrawableNodes,
    node_data: &Vec<f32>,
    triangle_dimensions: &[f32],
    triangle_tex_coords: &[f32],
) {
    // 2D node data must always consist of a (x, y) pair
    if node_data.len() % 2 != 0 {
        panic!("Error: Invalid node data. Length of node data is {}.", node_data.len());
    }

    let mut index = 0;
    for (i, x) in node_data.iter().enumerate().filter(|&(i, _)| i % 2 == 0) {
        let y = node_data[i + 1];

        // Fill vertex positions
        for (j, offset) in triangle_dimensions.iter().enumerate() {
            let pos = if j % 2 == 0 { x + offset } else { y + offset };
            drawable.vertices.push(pos);
        }

        // Fill indices
        drawable.indices.push(index as u16);
        drawable.indices.push(index as u16 + 1);
        drawable.indices.push(index as u16 + 2);

        // Fill texture coordinates
        for tex in triangle_tex_coords.iter() {
            drawable.tex_coords.push(*tex);
        }

        index += 3;
    }
}

/// Fills the vectors with the vertices and indices from the edge data set
fn fill_drawable_edges(
    drawable: &mut DrawableEdges,
    edge_data: &Vec<f32>,
    calc_square_dimension: &Fn(f32, f32, f32, f32) -> [f32; 8],
) {
    // 2D edge data must always consist of a (x, y) start and (x, y) end pair
    if edge_data.len() % 4 != 0 {
        panic!("Error: Invalid edge data. Length of edge data is {}.", edge_data.len());
    }

    let mut index = 0;
    for (i, start_x) in edge_data.iter().enumerate().filter(|&(i, _)| i % 4 == 0) {
        let start_y = edge_data[i + 1];
        let end_x = edge_data[i + 2];
        let end_y = edge_data[i + 3];

        let edge_positions = calc_square_dimension(*start_x, start_y, end_x, end_y);

        // Fill vertex positions
        for pos in edge_positions.iter() {
            drawable.vertices.push(*pos);
        }

        // Fill indices
        drawable.indices.push(index as u16);
        drawable.indices.push(index as u16 + 2);
        drawable.indices.push(index as u16 + 1);
        drawable.indices.push(index as u16 + 1);
        drawable.indices.push(index as u16 + 2);
        drawable.indices.push(index as u16 + 3);

        index += 4;
    }
}

#[cfg(test)]
mod tests {
    use crate::render::render_edges::{calc_edge_vertices, EDGE_THICKNESS_HALF};

    use super::*;

    /// Texture coordinates of a single vertex
    const TRIANGLE_TEX_COORDS: [f32; 6] = [1., 1., 1., 1., 1., 1.];

    /// Vertices offset for a single node
    const TRIANGLE_DIMENSIONS: [f32; 6] = [1., 1., 1., 1., 1., 1.];

    #[test]
    fn test_create_drawable_nodes() {
        let node_data: Vec<f32> = vec!(0., 0.);
        let drawable = create_drawable_nodes(&node_data,
                                             &TRIANGLE_DIMENSIONS);

        assert_eq!(drawable.vertices.len(), 0);
        assert_eq!(drawable.vertices.capacity(), 6);

        assert_eq!(drawable.indices.len(), 0);
        assert_eq!(drawable.indices.capacity(), 3);

        assert_eq!(drawable.tex_coords.len(), 0);
        assert_eq!(drawable.tex_coords.capacity(), 6);
    }

    #[test]
    fn test_create_drawable_edges() {
        let edge_data: Vec<f32> = vec!(0., 0., 1., 1.);
        let drawable = create_drawable_edges(&edge_data);

        assert_eq!(drawable.vertices.len(), 0);
        assert_eq!(drawable.vertices.capacity(), 8);

        assert_eq!(drawable.indices.len(), 0);
        assert_eq!(drawable.indices.capacity(), 6);
    }

    #[test]
    fn test_fill_drawable_nodes() {
        let mut drawable = DrawableNodes::default();
        let nodes: Vec<f32> = vec![0., 0.];

        fill_drawable_nodes(&mut drawable, &nodes,
                            &TRIANGLE_DIMENSIONS,
                            &TRIANGLE_TEX_COORDS);

        assert_eq!(drawable.vertices.len(), 6);
        assert_eq!(drawable.vertices[0], TRIANGLE_DIMENSIONS[0]);
        assert_eq!(drawable.vertices[1], TRIANGLE_DIMENSIONS[1]);
        assert_eq!(drawable.vertices[2], TRIANGLE_DIMENSIONS[2]);
        assert_eq!(drawable.vertices[3], TRIANGLE_DIMENSIONS[3]);
        assert_eq!(drawable.vertices[4], TRIANGLE_DIMENSIONS[4]);
        assert_eq!(drawable.vertices[5], TRIANGLE_DIMENSIONS[5]);

        assert_eq!(drawable.indices.len(), 3);
        assert_eq!(drawable.indices[0], 0);
        assert_eq!(drawable.indices[1], 1);
        assert_eq!(drawable.indices[2], 2);

        assert_eq!(drawable.tex_coords.len(), 6);
        assert_eq!(drawable.tex_coords[0], TRIANGLE_TEX_COORDS[0]);
        assert_eq!(drawable.tex_coords[1], TRIANGLE_TEX_COORDS[1]);
        assert_eq!(drawable.tex_coords[2], TRIANGLE_TEX_COORDS[2]);
        assert_eq!(drawable.tex_coords[3], TRIANGLE_TEX_COORDS[3]);
        assert_eq!(drawable.tex_coords[4], TRIANGLE_TEX_COORDS[4]);
        assert_eq!(drawable.tex_coords[5], TRIANGLE_TEX_COORDS[5]);
    }

    #[test]
    fn test_fill_drawable_edges() {
        let mut drawable = DrawableEdges::default();

        let edges: Vec<f32> = vec!(1., 1., 0., 0.);
        fill_drawable_edges(&mut drawable, &edges, &calc_edge_vertices);

        assert_eq!(drawable.vertices.len(), 8);
        assert_eq!(drawable.vertices[0], -EDGE_THICKNESS_HALF);
        assert_eq!(drawable.vertices[1], 0.);
        assert_eq!(drawable.vertices[2], EDGE_THICKNESS_HALF);
        assert_eq!(drawable.vertices[3], 0.);
        assert_eq!(drawable.vertices[4], -EDGE_THICKNESS_HALF);
        assert_eq!(drawable.vertices[5], 1.);
        assert_eq!(drawable.vertices[5], EDGE_THICKNESS_HALF);
        assert_eq!(drawable.vertices[5], 1.);

        assert_eq!(drawable.indices.len(), 6);
        assert_eq!(drawable.indices[0], 0);
        assert_eq!(drawable.indices[1], 1);
        assert_eq!(drawable.indices[2], 2);
        assert_eq!(drawable.indices[3], 1);
        assert_eq!(drawable.indices[4], 3);
        assert_eq!(drawable.indices[5], 2);
    }
}