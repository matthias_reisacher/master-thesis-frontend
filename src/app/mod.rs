use std::cell::RefCell;
use std::rc::Rc;

use self::data::Graph;
use self::state::State;

pub mod state;
pub mod data;

/// Used to instantiate our application and to store general information.
pub struct App {
    // JavaScript handlers require a static lifetime. Since this is rather unsafe, Rc and RefCell
    // are used as a workaround.
    pub state: Rc<RefCell<State>>,
    pub graph: Rc<RefCell<Graph>>
}

impl App {
    /// Create a new App
    pub fn new() -> App {
        App {
            state: Rc::new(RefCell::new(State::new())),
            graph: Rc::new(RefCell::new(Graph::new()))
        }
    }
}