use std::rc::Rc;

use wasm_bindgen::{JsCast, prelude::{Closure, JsValue}};
use web_sys::{HtmlCanvasElement, HtmlElement, WebGlRenderingContext, window};

use crate::app::{App, state::Msg};

pub static APP_DIV_ID: &'static str = "dago";
pub static CANVAS_ID: &'static str = "dago-canvas";

pub static CANVAS_WIDTH: i32 = 512;
pub static CANVAS_HEIGHT: i32 = 512;

/// Initializes the WebGL context
pub fn create_webgl_context(app: Rc<App>) -> Result<WebGlRenderingContext, JsValue> {
    let canvas = init_canvas(app)?;

    let gl: WebGlRenderingContext = canvas.get_context("webgl")?.unwrap().dyn_into()?;

    Ok(gl)
}

/// Creates a HTML canvas inside a container element
pub fn init_canvas(app: Rc<App>) -> Result<HtmlCanvasElement, JsValue> {
    let window = window().unwrap();
    let document = window.document().unwrap();

    // Create canvas element
    let canvas: HtmlCanvasElement = document.create_element("canvas").unwrap().dyn_into()?;

    canvas.set_width(CANVAS_WIDTH as u32);
    canvas.set_height(CANVAS_HEIGHT as u32);

    canvas.set_id(CANVAS_ID);
    canvas.style().set_property("margin", "auto")?;

    // Set controls
    attach_mouse_down_handler(&canvas, Rc::clone(&app))?;
    attach_mouse_up_handler(&canvas, Rc::clone(&app))?;
    attach_mouse_move_handler(&canvas, Rc::clone(&app))?;
    attach_mouse_wheel_handler(&canvas, Rc::clone(&app))?;

    attach_touch_start_handler(&canvas, Rc::clone(&app))?;
    attach_touch_move_handler(&canvas, Rc::clone(&app))?;
    attach_touch_end_handler(&canvas, Rc::clone(&app))?;

    // Create or use already existing canvas wrapper
    let app_div: HtmlElement = match document.get_element_by_id(APP_DIV_ID) {
        Some(container) => container.dyn_into()?,
        None => {
            let app_div = document.create_element("div")?;
            document.body().unwrap().append_child(&app_div)?;
            app_div.set_id(APP_DIV_ID);
            app_div.dyn_into()?
        }
    };

    app_div.style().set_property("display", "flex")?;
    app_div.append_child(&canvas)?;

    Ok(canvas)
}

/// Attach MouseDown-Event to JavaScript
fn attach_mouse_down_handler(
    canvas: &HtmlCanvasElement,
    app: Rc<App>,
) -> Result<(), JsValue> {
    let handler = move |event: web_sys::MouseEvent| {
        let x = event.client_x();
        let y = event.client_y();
        app.state.borrow_mut().msg(&Msg::MouseDown(x, y));
    };

    let handler = Closure::wrap(Box::new(handler) as Box<FnMut(_)>);

    canvas.add_event_listener_with_callback("mousedown", handler.as_ref().unchecked_ref())?;

    handler.forget();

    Ok(())
}

/// Attach MouseUp-Event to JavaScript
fn attach_mouse_up_handler(
    canvas: &HtmlCanvasElement,
    app: Rc<App>,
) -> Result<(), JsValue> {
    let handler = move |_event: web_sys::MouseEvent| {
        app.state.borrow_mut().msg(&Msg::MouseUp);
    };

    let handler = Closure::wrap(Box::new(handler) as Box<FnMut(_)>);

    canvas.add_event_listener_with_callback("mouseup", handler.as_ref().unchecked_ref())?;
    handler.forget();
    Ok(())
}

/// Attach MouseMove-Event to JavaScript
fn attach_mouse_move_handler(
    canvas: &HtmlCanvasElement,
    app: Rc<App>,
) -> Result<(), JsValue> {
    let handler = move |event: web_sys::MouseEvent| {
        event.prevent_default();
        let x = event.client_x();
        let y = event.client_y();
        app.state.borrow_mut().msg(&Msg::MouseMove(x, y));
    };

    let handler = Closure::wrap(Box::new(handler) as Box<FnMut(_)>);
    canvas.add_event_listener_with_callback("mousemove", handler.as_ref().unchecked_ref())?;
    handler.forget();

    Ok(())
}

/// Attach MouseWheel-Event to JavaScript
fn attach_mouse_wheel_handler(
    canvas: &HtmlCanvasElement,
    app: Rc<App>,
) -> Result<(), JsValue> {
    let handler = move |event: web_sys::WheelEvent| {
        event.prevent_default();

        let zoom_amount = event.delta_y();

        app.state.borrow_mut().msg(&Msg::Zoom(zoom_amount as f32));
    };

    let handler = Closure::wrap(Box::new(handler) as Box<FnMut(_)>);
    canvas.add_event_listener_with_callback("wheel", handler.as_ref().unchecked_ref())?;
    handler.forget();

    Ok(())
}

/// Attach TouchStart-Event to JavaScript to support touch-based UIs
fn attach_touch_start_handler(
    canvas: &HtmlCanvasElement,
    app: Rc<App>,
) -> Result<(), JsValue> {
    let handler = move |event: web_sys::TouchEvent| {
        let touch = event.touches().item(0).expect("First Touch");
        let x = touch.client_x();
        let y = touch.client_y();
        app.state.borrow_mut().msg(&Msg::MouseDown(x, y));
    };

    let handler = Closure::wrap(Box::new(handler) as Box<FnMut(_)>);
    canvas.add_event_listener_with_callback("touchstart", handler.as_ref().unchecked_ref())?;
    handler.forget();

    Ok(())
}

/// Attach TouchMove-Event to JavaScript to support touch-based UIs
fn attach_touch_move_handler(
    canvas: &HtmlCanvasElement,
    app: Rc<App>,
) -> Result<(), JsValue> {
    let handler = move |event: web_sys::TouchEvent| {
        event.prevent_default();
        let touch = event.touches().item(0).expect("First Touch");
        let x = touch.client_x();
        let y = touch.client_y();
        app.state.borrow_mut().msg(&Msg::MouseMove(x, y));
    };

    let handler = Closure::wrap(Box::new(handler) as Box<FnMut(_)>);
    canvas.add_event_listener_with_callback("touchmove", handler.as_ref().unchecked_ref())?;
    handler.forget();

    Ok(())
}

/// Attach TouchEnd-Event to JavaScript to support touch-based UIs
fn attach_touch_end_handler(
    canvas: &HtmlCanvasElement,
    app: Rc<App>,
) -> Result<(), JsValue> {
    let handler = move |_event: web_sys::TouchEvent| {
        app.state.borrow_mut().msg(&Msg::MouseUp);
    };

    let handler = Closure::wrap(Box::new(handler) as Box<FnMut(_)>);

    canvas.add_event_listener_with_callback("touchend", handler.as_ref().unchecked_ref())?;

    handler.forget();

    Ok(())
}