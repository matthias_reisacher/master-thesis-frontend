use core::borrow::{Borrow, BorrowMut};
use std::cell::RefCell;
use std::collections::HashMap;

use js_sys::Reflect;
use web_sys::{WebGlRenderingContext, WebGlRenderingContext as GL};

use crate::app::data::Graph;
use crate::app::state::State;
use crate::canvas::{CANVAS_HEIGHT, CANVAS_WIDTH};
use crate::shader::{ShaderKind, ShaderSystem};

use self::render_edges::RenderableEdgeLines;
use self::render_nodes::RenderableNodeTriangles;
use self::render_trait::Render;

pub mod texture_unit;
pub mod render_nodes;
pub mod render_edges;
mod render_trait;

struct VaoExtension {
    oes_vao_ext: js_sys::Object,
    vaos: RefCell<HashMap<String, Vao>>,
}

struct Vao(js_sys::Object);

/// Used to instantiate our application.
pub struct WebRenderer {
    shader_sys: ShaderSystem,
    vao_ext: VaoExtension,
}

impl WebRenderer {
    /// Create a new WebRenderer.
    pub fn new(gl: &WebGlRenderingContext) -> WebRenderer {
        let shader_sys = ShaderSystem::new(&gl);

        // Enables OES_vertex_array_object
        let oes_vao_ext = gl
            .get_extension("OES_vertex_array_object")
            .expect("Get OES vao ext")
            .expect("OES vao ext");

        let vao_ext = VaoExtension {
            oes_vao_ext,
            vaos: RefCell::new(HashMap::new()),
        };


        WebRenderer { shader_sys, vao_ext }
    }

    /// Deletes all Vertex Array Objects.
    pub fn clear(&mut self) {
        for (_, vao) in self.vao_ext.borrow().vaos.borrow().iter() {
            self.delete_vao(vao);
        }

        self.vao_ext.borrow_mut().vaos = RefCell::new(HashMap::new());
    }

    /// Render all components.
    pub fn render(
        &mut self,
        gl: &WebGlRenderingContext,
        state: &State,
        graph: &Graph,
        width: Option<i32>,
        height: Option<i32>,
    ) {
        let width = width.unwrap_or(CANVAS_WIDTH);
        let height = height.unwrap_or(CANVAS_HEIGHT);

        gl.clear_color(0., 0., 0., 1.);
        gl.clear(GL::COLOR_BUFFER_BIT | GL::DEPTH_BUFFER_BIT);

        gl.viewport(0, 0, width, height);

        self.render_edges(gl, state, graph);
        self.render_nodes(gl, state, graph);
    }

    /// Renders the node-components.
    fn render_nodes(
        &mut self,
        gl: &WebGlRenderingContext,
        state: &State,
        graph: &Graph,
    ) {
        gl.bind_framebuffer(GL::FRAMEBUFFER, None);

        let node_shader = self.shader_sys.get_shader(&ShaderKind::Node).unwrap();
        self.shader_sys.use_program(gl, ShaderKind::Node);

        let nodes = RenderableNodeTriangles::new(node_shader);

        self.prepare_for_render(gl, &nodes, graph, "nodes");
        nodes.render(gl, state, graph);
    }

    /// Renders the edge-components.
    fn render_edges(
        &mut self,
        gl: &WebGlRenderingContext,
        state: &State,
        graph: &Graph,
    ) {
        gl.bind_framebuffer(GL::FRAMEBUFFER, None);

        let edge_shader = self.shader_sys.get_shader(&ShaderKind::Edge).unwrap();
        self.shader_sys.use_program(gl, ShaderKind::Edge);

        let edges = RenderableEdgeLines::new(edge_shader);

        self.prepare_for_render(gl, &edges, graph, "edges");
        edges.render(gl, state, graph);
    }

    /// Creates and binds VAO for render call.
    fn prepare_for_render<'a>(
        &self,
        gl: &WebGlRenderingContext,
        renderable: &impl Render<'a>,
        graph: &Graph,
        key: &str,
    ) {
        if self.vao_ext.vaos.borrow().get(key).is_none() {
            let vao = self.create_vao();
            self.bind_vao(&vao);
            renderable.buffer_attributes(gl, graph);
            self.vao_ext.vaos.borrow_mut().insert(key.to_string(), vao);
            return;
        }

        let vaos = self.vao_ext.vaos.borrow();
        let vao = vaos.get(key).unwrap();
        self.bind_vao(vao);
    }

    /// Creates a new VAO.
    fn create_vao(&self) -> Vao {
        let oes_vao_ext = &self.vao_ext.oes_vao_ext;

        let create_vao_ext = Reflect::get(oes_vao_ext, &"createVertexArrayOES".into())
            .expect("Create vao func")
            .into();

        Vao(
            Reflect::apply(&create_vao_ext, oes_vao_ext, &js_sys::Array::new())
                .expect("Created vao")
                .into(),
        )
    }

    /// Binds the passed VAO.
    fn bind_vao(
        &self,
        vao: &Vao,
    ) {
        let oes_vao_ext = &self.vao_ext.oes_vao_ext;

        let bind_vao_ext = Reflect::get(&oes_vao_ext, &"bindVertexArrayOES".into())
            .expect("Create vao func")
            .into();

        let args = js_sys::Array::new();
        args.push(&vao.0);

        Reflect::apply(&bind_vao_ext, oes_vao_ext, &args).expect("Bound VAO");
    }

    /// Deletes the given VAO.
    fn delete_vao(
        &self,
        vao: &Vao,
    ) {
        let oes_vao_ext = &self.vao_ext.oes_vao_ext;

        let delete_vao_ext = Reflect::get(&oes_vao_ext, &"deleteVertexArrayOES".into())
            .expect("Create vao func")
            .into();

        let args = js_sys::Array::new();
        args.push(&vao.0);

        Reflect::apply(&delete_vao_ext, oes_vao_ext, &args).expect("Delete VAO");
    }
}