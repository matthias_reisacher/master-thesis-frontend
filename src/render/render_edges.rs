use nalgebra::{Isometry3, Vector2, zero};
use web_sys::{WebGlRenderingContext, WebGlRenderingContext as GL};

use crate::app::data::Graph;
use crate::app::data::preprocessor::DrawableEdges;
use crate::app::state::State;
use crate::render::render_trait::Render;
use crate::shader::{Shader, ShaderKind};

/// Half of the edge's thickness
pub static EDGE_THICKNESS_HALF: f32 = 0.01;

/// Calculates edge vertices to form a rectangular object by adding an offset in the
/// direction of the lines normals.
/// For a clockwise triangle structure, use following indices:
/// indices = [0, 2, 1, 1, 2, 3]
pub fn calc_edge_vertices(
    start_x: f32,
    start_y: f32,
    end_x: f32,
    end_y: f32,
) -> [f32; 8] {
    // Calculate normals and resize it to half of the edge's thickness
    let mut normals = calc_normalized_normals(start_x, start_y, end_x, end_y);
    normals = (normals.0 * EDGE_THICKNESS_HALF, normals.1 * EDGE_THICKNESS_HALF);

    // Find the actual start and end point of the render conform edge
    let edge = find_edge_start_for_rendering(start_x, start_y, end_x, end_y);

    [
        (edge.0).x + (normals.0).x, (edge.0).y + (normals.0).y,
        (edge.0).x + (normals.1).x, (edge.0).y + (normals.1).y,
        (edge.1).x + (normals.0).x, (edge.1).y + (normals.0).y,
        (edge.1).x + (normals.1).x, (edge.1).y + (normals.1).y,
    ]
}

/// Calculates and returns the normalized normals of the given edge.
/// The first normal is either directed to the left side of the coordinate system, or to the top
/// when the edge is flat.
fn calc_normalized_normals(
    start_x: f32,
    start_y: f32,
    end_x: f32,
    end_y: f32,
) -> (Vector2<f32>, Vector2<f32>) {
    let dx = end_x - start_x;
    let dy = end_y - start_y;

    let norm_1: Vector2<f32> = Vector2::new(-dy, dx).normalize();
    let norm_2: Vector2<f32> = Vector2::new(dy, -dx).normalize();

    if norm_1.x < norm_2.x {
        (norm_1, norm_2)
    } else if norm_1.x > norm_2.x {
        (norm_2, norm_1)
    } else {
        // edge is flat => differ by y-value
        if norm_1.y >= norm_2.y {
            (norm_1, norm_2)
        } else {
            (norm_2, norm_1)
        }
    }
}

/// Since we are always rendering from left to right (or top to bottom if the line is vertical),
/// we have to find the "actual" start point of the edge.
fn find_edge_start_for_rendering(
    start_x: f32,
    start_y: f32,
    end_x: f32,
    end_y: f32,
) -> (Vector2<f32>, Vector2<f32>) {
    let start = Vector2::new(start_x, start_y);
    let end = Vector2::new(end_x, end_y);

    if start_x < end_x {
        (start, end)
    } else if start_x > end_x {
        (end, start)
    } else {
        // Vertical edge
        if start_y < end_y {
            (start, end)
        } else {
            (end, start)
        }
    }
}

/// Represents the renderable node objects.
pub struct RenderableEdgeLines<'a> {
    shader: &'a Shader,
}

impl<'a> RenderableEdgeLines<'a> {
    /// Creates a new Node
    pub fn new(shader: &'a Shader) -> RenderableEdgeLines<'a> {
        RenderableEdgeLines { shader }
    }

    /// Bind the given vertex data to the WebGl buffer
    fn bind_vertex_data(
        &self,
        gl: &WebGlRenderingContext,
        vertices: &[f32],
    ) {
        let a_position = gl.get_attrib_location(&self.shader.program, "a_position");
        gl.enable_vertex_attrib_array(a_position as u32);

        RenderableEdgeLines::buffer_f32_data(&gl, &vertices, a_position as u32, 2);
    }

    /// Bind the given index data to the WebGl buffer
    fn bind_index_data(&self, gl: &WebGlRenderingContext, indices: &[u16]) {
        RenderableEdgeLines::buffer_u16_indices(&gl, &indices);
    }
}

impl<'a> Render<'a> for RenderableEdgeLines<'a> {
    fn shader_kind() -> ShaderKind {
        ShaderKind::Node
    }

    fn shader(&'a self) -> &'a Shader {
        &self.shader
    }

    fn buffer_attributes(
        &self,
        gl: &WebGlRenderingContext,
        graph: &Graph,
    ) {
        let drawable_default = DrawableEdges::default();

        let drawable: &DrawableEdges;
        match graph.get_edges().get_data() {
            Some(data) => drawable = data,
            None => drawable = &drawable_default,
        };

        self.bind_vertex_data(gl, drawable.get_edges());
        self.bind_index_data(gl, drawable.get_indices());
    }

    fn render(
        &self,
        gl: &WebGlRenderingContext,
        state: &State,
        graph: &Graph,
    ) {
        let shader = self.shader();

        let u_mvp = shader.get_uniform_location(gl, "u_mvp");

        let model = Isometry3::new(zero(), zero());
        let view = state.camera().view();
        let projection = state.camera().projection();

        let mut mvp = RenderableEdgeLines::calc_mvp(&model, &view, projection);
        gl.uniform_matrix4fv_with_f32_array(u_mvp.as_ref(), false, &mut mvp);

        let n_indices;
        match graph.get_edges().get_data() {
            Some(data) => n_indices = data.get_indices().len(),
            None => n_indices = 0,
        };

        gl.draw_elements_with_i32(GL::TRIANGLES, n_indices as i32, GL::UNSIGNED_SHORT, 0);
    }
}

#[cfg(test)]
mod tests {
    use float_cmp::ApproxEq;

    use super::*;

    #[test]
    fn test_calc_normalized_normals() {
        // From left to right
        let normals = calc_normalized_normals(0., 2., 5., 2.);
        assert_eq!((normals.0).x, 0.);
        assert_eq!((normals.0).y, 1.);
        assert_eq!((normals.1).x, 0.);
        assert_eq!((normals.1).y, -1.);

        // From right to left
        let normals = calc_normalized_normals(5., 2., 0., 2.);
        assert_eq!((normals.0).x, 0.);
        assert_eq!((normals.0).y, 1.);
        assert_eq!((normals.1).x, 0.);
        assert_eq!((normals.1).y, -1.);

        // From top to bottom
        let normals = calc_normalized_normals(3., 5., 3., -2.);
        assert_eq!((normals.0).x, -1.);
        assert_eq!((normals.0).y, 0.);
        assert_eq!((normals.1).x, 1.);
        assert_eq!((normals.1).y, 0.);

        // From bottom to top
        let normals = calc_normalized_normals(2., -2., 2., 5.);
        assert_eq!((normals.0).x, -1.);
        assert_eq!((normals.0).y, 0.);
        assert_eq!((normals.1).x, 1.);
        assert_eq!((normals.1).y, 0.);

        // From bottom left to top right
        let normals = calc_normalized_normals(-2., -4., 0., 4.);
        assert!(approx_eq!(f32, (normals.0).x, - 0.970143, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.0).y, 0.242536, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.1).x, 0.970143, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.1).y, - 0.242536, epsilon = 0.000001));

        // From top right to bottom left
        let normals = calc_normalized_normals(1., 5., 0., -10.);
        assert!(approx_eq!(f32, (normals.0).x, - 0.997785, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.0).y, 0.066519, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.1).x, 0.997785, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.1).y, - 0.066519, epsilon = 0.000001));

        // From bottom right to top left
        let normals = calc_normalized_normals(3., 1., 2., 2.);
        assert!(approx_eq!(f32, (normals.0).x, - 0.707107, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.0).y, - 0.707107, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.1).x, 0.707107, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.1).y, 0.707107, epsilon = 0.000001));

        // From top left to bottom right
        let normals = calc_normalized_normals(2., 2., 3., 1.);
        assert!(approx_eq!(f32, (normals.0).x, - 0.707107, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.0).y, - 0.707107, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.1).x, 0.707107, epsilon = 0.000001));
        assert!(approx_eq!(f32, (normals.1).y, 0.707107, epsilon = 0.000001));
    }

    #[test]
    fn test_find_edge_start_for_rendering() {
        // From left to right
        let normals = find_edge_start_for_rendering(0., 2., 5., 2.);
        assert_eq!((normals.0).x, 0.);
        assert_eq!((normals.0).y, 2.);
        assert_eq!((normals.1).x, 5.);
        assert_eq!((normals.1).y, 2.);

        // From right to left
        let normals = find_edge_start_for_rendering(5., 2., 0., 2.);
        assert_eq!((normals.0).x, 0.);
        assert_eq!((normals.0).y, 2.);
        assert_eq!((normals.1).x, 5.);
        assert_eq!((normals.1).y, 2.);

        // From top to bottom
        let normals = find_edge_start_for_rendering(3., 5., 3., -2.);
        assert_eq!((normals.0).x, 3.);
        assert_eq!((normals.0).y, -2.);
        assert_eq!((normals.1).x, 3.);
        assert_eq!((normals.1).y, 5.);

        // From bottom to top
        let normals = find_edge_start_for_rendering(2., -2., 2., 5.);
        assert_eq!((normals.0).x, 2.);
        assert_eq!((normals.0).y, -2.);
        assert_eq!((normals.1).x, 2.);
        assert_eq!((normals.1).y, 5.);

        // From bottom left to top right
        let normals = find_edge_start_for_rendering(-2., -4., 0., 4.);
        assert_eq!((normals.0).x, -2.);
        assert_eq!((normals.0).y, -4.);
        assert_eq!((normals.1).x, 0.);
        assert_eq!((normals.1).y, 4.);

        // From top right to bottom left
        let normals = find_edge_start_for_rendering(1., 5., 0., -10.);
        assert_eq!((normals.0).x, 0.);
        assert_eq!((normals.0).y, -10.);
        assert_eq!((normals.1).x, 1.);
        assert_eq!((normals.1).y, 5.);

        // From bottom right to top left
        let normals = find_edge_start_for_rendering(3., 1., 2., 2.);
        assert_eq!((normals.0).x, 2.);
        assert_eq!((normals.0).y, 2.);
        assert_eq!((normals.1).x, 3.);
        assert_eq!((normals.1).y, 1.);

        // From top left to bottom right
        let normals = find_edge_start_for_rendering(2., 2., 3., 1.);
        assert_eq!((normals.0).x, 2.);
        assert_eq!((normals.0).y, 2.);
        assert_eq!((normals.1).x, 3.);
        assert_eq!((normals.1).y, 1.);
    }
}