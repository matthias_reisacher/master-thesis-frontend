use web_sys::WebGlRenderingContext as GL;

#[derive(Clone, Copy)]
pub enum TextureUnit {
    Node = 0,
}

impl TextureUnit {
    /// Helper for `gl.active_texture`
    #[allow(non_snake_case)]
    pub fn TEXTURE_N(&self) -> u32 {
        match self {
            TextureUnit::Node => GL::TEXTURE0,
        }
    }

    /// Helper for `gl.uniform1i` calls
    pub fn texture_unit(&self) -> i32 {
        *self as i32
    }
}