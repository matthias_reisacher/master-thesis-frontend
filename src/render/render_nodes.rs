use nalgebra::{Isometry3, zero};
use web_sys::{WebGlRenderingContext, WebGlRenderingContext as GL};

use crate::app::data::Graph;
use crate::app::data::preprocessor::DrawableNodes;
use crate::app::state::State;
use crate::render::{render_trait::Render, texture_unit::TextureUnit};
use crate::shader::{Shader, ShaderKind};

/// Texture coordinates of a single vertex
pub const TEX_COORDS: [f32; 6] = [
    1., 1., // Top right
    0., 1., // Top left
    0.5, 0., // Bottom mid
];

/// Vertices offset for a single node
static mut TRIANGLE_VERTICES: Option<[f32; 6]> = Option::None;

/// The height of a single triangle vertex
const TRIANGE_VERTEX_HEIGHT: f32 = 0.1;

// Calculate vertices if not already set
pub unsafe fn get_triangle_vertices() -> &'static [f32] {
    if TRIANGLE_VERTICES.is_none() {
        TRIANGLE_VERTICES = Some(calc_triangle_vertices(TRIANGE_VERTEX_HEIGHT));
    }

    TRIANGLE_VERTICES.as_ref().unwrap()
}

/// Calculates the triangle vertices.
///
/// # Calculation of dimensions - equilateral triangle
/// Side length: a = h * 2 / sqrt(3)
/// Distance bottom: db = sqrt(3) / 3 * a
/// Distance top: dt = h - db
/// Distance side: ds = a / 2
fn calc_triangle_vertices(h: f32) -> [f32; 6] {
    let a = h * 2. / f32::sqrt(3.);
    let db = f32::sqrt(3.) / 3. * a;
    let dt = h - db;
    let ds = a / 2.;

    [
        -ds, dt, // Top left
        ds, dt, // Top right
        0., -db, // Bottom mid
    ]
}

/// Represents the renderable node objects.
pub struct RenderableNodeTriangles<'a> {
    shader: &'a Shader,
}

impl<'a> RenderableNodeTriangles<'a> {
    /// Creates a new Node
    pub fn new(shader: &'a Shader) -> RenderableNodeTriangles<'a> {
        RenderableNodeTriangles { shader }
    }

    /// Bind the given vertex data to the WebGl buffer
    fn bind_vertex_data(&self, gl: &WebGlRenderingContext, vertices: &[f32]) {
        let a_position = gl.get_attrib_location(&self.shader.program, "a_position");
        gl.enable_vertex_attrib_array(a_position as u32);

        RenderableNodeTriangles::buffer_f32_data(&gl, &vertices, a_position as u32, 2);
    }

    /// Bind the given index data to the WebGl buffer
    fn bind_index_data(&self, gl: &WebGlRenderingContext, indices: &[u16]) {
        RenderableNodeTriangles::buffer_u16_indices(&gl, &indices);
    }

    /// Bind the given texture coordinates to the WebGl buffer
    fn bind_texture_data(&self, gl: &WebGlRenderingContext, tex_coords: &[f32]) {
        let a_texture_coord = gl.get_attrib_location(&self.shader.program, "a_textureCoord");
        gl.enable_vertex_attrib_array(a_texture_coord as u32);

        RenderableNodeTriangles::buffer_f32_data(&gl, &tex_coords, a_texture_coord as u32, 2);
    }
}

impl<'a> Render<'a> for RenderableNodeTriangles<'a> {
    fn shader_kind() -> ShaderKind {
        ShaderKind::Node
    }

    fn shader(&'a self) -> &'a Shader {
        &self.shader
    }

    fn buffer_attributes(&self, gl: &WebGlRenderingContext, graph: &Graph) {
        let drawable_default = DrawableNodes::default();

        let drawable: &DrawableNodes;
        match graph.get_nodes().get_data() {
            Some(data) => drawable = data,
            None => drawable = &drawable_default,
        };

        self.bind_vertex_data(gl, drawable.get_vertices());
        self.bind_index_data(gl, drawable.get_indices());
        self.bind_texture_data(gl, drawable.get_tex_coords());
    }

    fn render(&self, gl: &WebGlRenderingContext, state: &State, graph: &Graph) {
        let shader = self.shader();

        let u_mvp = shader.get_uniform_location(gl, "u_mvp");
        let u_node_texture = shader.get_uniform_location(gl, "u_nodeTexture");

        let model = Isometry3::new(zero(), zero());
        let view = state.camera().view();
        let projection = state.camera().projection();

        let mut mvp = RenderableNodeTriangles::calc_mvp(&model, &view, projection);
        gl.uniform_matrix4fv_with_f32_array(u_mvp.as_ref(), false, &mut mvp);

        gl.uniform1i(u_node_texture.as_ref(), TextureUnit::Node.texture_unit());

        let n_indices;
        match graph.get_nodes().get_data() {
            Some(data) => n_indices = data.get_indices().len(),
            None => n_indices = 0,
        };

        // Enable alpha blending
        gl.enable(GL::BLEND);
        gl.blend_func(GL::SRC_ALPHA, GL::ONE_MINUS_SRC_ALPHA);

        gl.draw_elements_with_i32(GL::TRIANGLES, n_indices as i32, GL::UNSIGNED_SHORT, 0);
    }
}
