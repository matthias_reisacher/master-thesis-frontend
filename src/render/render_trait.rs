use js_sys::WebAssembly;
use wasm_bindgen::JsCast;
use web_sys::{WebGlRenderingContext, WebGlRenderingContext as GL};

use crate::app::data::Graph;
use crate::app::state::State;
use crate::nalgebra::{Isometry3, Matrix4, Perspective3};
use crate::shader::{Shader, ShaderKind};

/// A renderable component.
pub trait Render<'a> {
    /// Returns the used ShaderKind.
    fn shader_kind() -> ShaderKind;

    /// Returns the used shader object.
    fn shader(&'a self) -> &'a Shader;

    /// Sets the attributes for a VAO.
    fn buffer_attributes(
        &self,
        gl: &WebGlRenderingContext,
        graph: &Graph,
    );

    /// Renders the component.
    fn render(
        &self,
        gl: &WebGlRenderingContext,
        state: &State,
        graph: &Graph,
    );

    /// Binds the data to a WebGL buffer.
    fn buffer_f32_data(
        gl: &WebGlRenderingContext,
        data: &[f32],
        attrib: u32,
        size: i32,
    ) {
        let memory_buffer = wasm_bindgen::memory()
            .dyn_into::<WebAssembly::Memory>()
            .unwrap()
            .buffer();

        let data_location = data.as_ptr() as u32 / 4;

        let data_array = js_sys::Float32Array::new(&memory_buffer)
            .subarray(data_location, data_location + data.len() as u32);

        let buffer = gl.create_buffer().unwrap();

        gl.bind_buffer(GL::ARRAY_BUFFER, Some(&buffer));
        gl.buffer_data_with_array_buffer_view(GL::ARRAY_BUFFER, &data_array, GL::STATIC_DRAW);
        gl.vertex_attrib_pointer_with_i32(attrib, size, GL::FLOAT, false, 0, 0);
    }

    /// Binds the data to a WebGL buffer.
    fn buffer_u8_data(
        gl: &WebGlRenderingContext,
        data: &[u8],
        attrib: u32,
        size: i32,
    ) {
        let memory_buffer = wasm_bindgen::memory()
            .dyn_into::<WebAssembly::Memory>()
            .unwrap()
            .buffer();

        let data_location = data.as_ptr() as u32;

        let data_array = js_sys::Uint8Array::new(&memory_buffer)
            .subarray(data_location, data_location + data.len() as u32);

        let buffer = gl.create_buffer().unwrap();

        gl.bind_buffer(GL::ARRAY_BUFFER, Some(&buffer));
        gl.buffer_data_with_array_buffer_view(GL::ARRAY_BUFFER, &data_array, GL::STATIC_DRAW);
        gl.vertex_attrib_pointer_with_i32(attrib, size, GL::UNSIGNED_BYTE, false, 0, 0);
    }

    /// Binds the data to a WebGL buffer.
    fn buffer_u16_indices(
        gl: &WebGlRenderingContext,
        indices: &[u16],
    ) {
        let memory_buffer = wasm_bindgen::memory()
            .dyn_into::<WebAssembly::Memory>()
            .unwrap()
            .buffer();

        let indices_location = indices.as_ptr() as u32 / 2;
        let indices_array = js_sys::Uint16Array::new(&memory_buffer)
            .subarray(indices_location, indices_location + indices.len() as u32);

        let index_buffer = gl.create_buffer().unwrap();
        gl.bind_buffer(GL::ELEMENT_ARRAY_BUFFER, Some(&index_buffer));
        gl.buffer_data_with_array_buffer_view(GL::ELEMENT_ARRAY_BUFFER, &indices_array, GL::STATIC_DRAW);
    }

    /// Calculates the Model View Projection Matrix
    fn calc_mvp(
        model: &Isometry3<f32>,
        view: &Isometry3<f32>,
        projection: &Perspective3<f32>,
    ) -> [f32; 16] {
        // The combination of the model with the view is still an isometry.
        let model_view: Isometry3<f32> = view * model;

        // Convert everything to a `Matrix4` so that they can be combined.
        let mat_model_view: Matrix4<f32> = model_view.to_homogeneous();

        // Combine everything.
        let model_view_projection: Matrix4<f32> = projection.as_matrix() * mat_model_view;

        let mut mvp = [0.; 16];
        mvp.copy_from_slice(model_view_projection.as_slice());

        mvp
    }
}