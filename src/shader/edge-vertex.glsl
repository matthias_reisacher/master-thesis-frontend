attribute highp vec2 a_position;

uniform highp mat4 u_mvp;

void main() {
    gl_Position = u_mvp * vec4(a_position.x, a_position.y, 0.0, 1.0);
}
