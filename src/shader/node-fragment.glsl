uniform sampler2D u_nodeTexture;

varying mediump vec2 v_textureCoord;

void main() {
    gl_FragColor = texture2D(u_nodeTexture, v_textureCoord);
}
