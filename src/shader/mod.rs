use std::cell::RefCell;
use std::collections::HashMap;

use wasm_bindgen::prelude::JsValue;
use web_sys::{WebGlProgram, WebGlRenderingContext, WebGlRenderingContext as GL, WebGlShader, WebGlUniformLocation};

use crate::utils::js::{debug, error};

static NODE_VS: &'static str = include_str!("node-vertex.glsl");
static NODE_FS: &'static str = include_str!("node-fragment.glsl");

static EDGE_VS: &'static str = include_str!("edge-vertex.glsl");
static EDGE_FS: &'static str = include_str!("edge-fragment.glsl");

/// Retrieving and manages all shaders.
pub struct ShaderSystem {
    programs: HashMap<ShaderKind, Shader>,
    active_program: RefCell<ShaderKind>,
}

impl ShaderSystem {
    /// Create  a new ShaderSystem.
    pub fn new(gl: &WebGlRenderingContext) -> ShaderSystem {
        let mut programs = HashMap::new();

        // Create a shader for each ShaderKind
        let node_shader = Shader::new(&gl, NODE_VS, NODE_FS, "node_shader").unwrap();
        let edge_shader = Shader::new(&gl, EDGE_VS, EDGE_FS, "edge_shader").unwrap();

        // Activate the default shader
        let active_program = RefCell::new(ShaderKind::Node);
        gl.use_program(Some(&node_shader.program));

        // Store a shader program for each ShaderKind
        programs.insert(ShaderKind::Node, node_shader);
        programs.insert(ShaderKind::Edge, edge_shader);

        ShaderSystem {
            programs,
            active_program,
        }
    }

    /// Returns a shader for the given kind.
    pub fn get_shader(
        &self,
        shader_kind: &ShaderKind,
    ) -> Option<&Shader> {
        self.programs.get(shader_kind)
    }

    /// Use a shader program. We cache the last used shader program to avoid unnecessary
    /// calls to the GPU.
    pub fn use_program(
        &self,
        gl: &WebGlRenderingContext,
        shader_kind: ShaderKind,
    ) {
        if *self.active_program.borrow() == shader_kind {
            return;
        }

        gl.use_program(Some(&self.programs.get(&shader_kind).unwrap().program));
        *self.active_program.borrow_mut() = shader_kind;
    }
}

/// Identifiers for our different shaders.
#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
pub enum ShaderKind {
    Node,
    Edge
}

/// One per ShaderKind.
pub struct Shader {
    pub program: WebGlProgram,
    uniforms: RefCell<HashMap<String, WebGlUniformLocation>>,
}

impl Shader {
    /// Create a new Shader program from a vertex and fragment shader.
    fn new(gl: &WebGlRenderingContext,
           vert_shader: &str,
           frag_shader: &str,
           name: &str
    ) -> Result<Shader, JsValue> {
        let identifier = create_shader_type_name(name, "vert");
        let vert_shader = compile_shader(&gl, GL::VERTEX_SHADER, vert_shader, &identifier)?;

        let identifier = create_shader_type_name(name, "frag");
        let frag_shader = compile_shader(&gl, GL::FRAGMENT_SHADER, frag_shader, &identifier)?;

        let program = link_program(&gl, &vert_shader, &frag_shader)?;

        let uniforms = RefCell::new(HashMap::new());

        Ok(Shader { program, uniforms })
    }

    /// Get the location of a uniform. The uniform will be cached at the first retrieval.
    pub fn get_uniform_location(
        &self,
        gl: &WebGlRenderingContext,
        uniform_name: &str,
    ) -> Option<WebGlUniformLocation> {
        let mut uniforms = self.uniforms.borrow_mut();

        if uniforms.get(uniform_name).is_none() {
            uniforms.insert(
                uniform_name.to_string(),
                gl.get_uniform_location(&self.program, uniform_name)
                    .expect(&format!("Uniform '{}' not found", uniform_name)),
            );
        }

        Some(uniforms.get(uniform_name).expect("loc").clone())
    }
}

/// Create a shader program using the WebGL APIs.
fn compile_shader(
    gl: &WebGlRenderingContext,
    shader_type: u32,
    source: &str,
    identifier: &str
) -> Result<WebGlShader, String> {
    let shader = gl
        .create_shader(shader_type)
        .ok_or_else(|| String::from("Could not create shader"))?;
    gl.shader_source(&shader, source);
    gl.compile_shader(&shader);
    if gl
        .get_shader_parameter(&shader, GL::COMPILE_STATUS)
        .as_bool()
        .unwrap_or(false)
    {
        debug(&format!("Successfully compiled shader '{}'", identifier));
        Ok(shader)
    } else {
        error(&format!("Could not compile shader '{}'", identifier));
        Err(gl
            .get_shader_info_log(&shader)
            .unwrap_or_else(|| String::from("Unknown error creating shader")))
    }
}

/// Link a shader program using the WebGL APIs.
fn link_program(
    gl: &WebGlRenderingContext,
    vert_shader: &WebGlShader,
    frag_shader: &WebGlShader,
) -> Result<WebGlProgram, String> {
    let program = gl
        .create_program()
        .ok_or_else(|| String::from("Unable to create shader program"))?;

    gl.attach_shader(&program, &vert_shader);
    gl.attach_shader(&program, &frag_shader);

    gl.link_program(&program);

    if gl
        .get_program_parameter(&program, GL::LINK_STATUS)
        .as_bool()
        .unwrap_or(false)
    {
        Ok(program)
    } else {
        Err(gl
            .get_program_info_log(&program)
            .unwrap_or_else(|| String::from("Unknown error creating program")))
    }
}

fn create_shader_type_name(shader_name: &str, shader_type: &str) -> String {
    format!("{}.{}", shader_name, shader_type)
}