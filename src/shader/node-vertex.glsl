attribute highp vec2 a_position;
attribute lowp vec2 a_textureCoord;

uniform highp mat4 u_mvp;

varying vec2 v_textureCoord;

void main() {
    v_textureCoord = a_textureCoord;
    gl_Position = u_mvp * vec4(a_position.x, a_position.y, 0.0, 1.0);
}
