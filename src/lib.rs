extern crate console_error_panic_hook;
extern crate core;
extern crate float_cmp;
extern crate js_sys;
extern crate nalgebra;
extern crate wasm_bindgen;
extern crate web_sys;

use std::rc::Rc;

use wasm_bindgen::prelude::{JsValue, wasm_bindgen};
use web_sys::WebGlRenderingContext;

use utils::js::debug;

use crate::app::App;
use crate::render::{texture_unit::TextureUnit, WebRenderer};

use self::canvas::create_webgl_context;
use self::load_texture_image::load_texture_image;
use self::utils::set_panic_hook;

mod shader;
mod render;
mod utils;
mod app;
mod canvas;
mod load_texture_image;

#[wasm_bindgen]
pub struct WebClient {
    app: Rc<App>,
    gl: Rc<WebGlRenderingContext>,
    renderer: WebRenderer,
}

#[wasm_bindgen]
impl WebClient {
    /// Creates a new web client
    #[wasm_bindgen(constructor)]
    pub fn new() -> WebClient {
        set_panic_hook();

        let app = Rc::new(App::new());
        let gl = Rc::new(create_webgl_context(Rc::clone(&app)).unwrap());
        let renderer = WebRenderer::new(&gl);

        WebClient { app, gl, renderer }
    }

    /// Loads required assets and prepares for starting
    pub fn start(&self) -> Result<(), JsValue> {
        // Load textures
        load_texture_image(Rc::clone(&self.gl), "node_texture.png", TextureUnit::Node);

        Ok(())
    }

    /// Render the scene
    pub fn render(
        &mut self,
        width: Option<i32>,
        height: Option<i32>,
    ) -> Result<(), JsValue> {
        self.renderer.render(&self.gl, &self.app.state.borrow(),
                             &self.app.graph.borrow(), width, height);
        Ok(())
    }

    /// Update the camera for the given canvas size
    pub fn update_canvas_size(
        &mut self,
        width: i32,
        height: i32,
    ) {
        let aspect = width as f32 / height as f32;
        let camera = Rc::clone(&self.app.state);
        (*camera).borrow_mut().new_camera(aspect);
    }

    /// Sets the node diagram data
    pub fn set_data(
        &mut self,
        nodes: Vec<f32>,
        edges: Vec<f32>,
    ) -> Result<(), JsValue> {
        debug(&format!("Set graph data:\n# Nodes = {}\n# Edges = {}", nodes.len() / 2,
                       edges.len() / 4));

        self.renderer.clear();

        let graph = Rc::clone(&self.app.graph);
        (*graph).borrow_mut().clear();
        (*graph).borrow_mut().set_data(nodes, edges);
        Ok(())
    }
}